ZSH_THEME_GIT_PROMPT_ADDED="%{$fg[red]%}+"
ZSH_THEME_GIT_PROMPT_MODIFIED="%{$fg[red]%}&"
ZSH_THEME_GIT_PROMPT_DELETED="%{$fg[red]%}-"
ZSH_THEME_GIT_PROMPT_RENAMED="%{$fg[red]%}±"
ZSH_THEME_GIT_PROMPT_UNMERGED="%{$fg[red]%}="
ZSH_THEME_GIT_PROMPT_UNTRACKED="%{$fg[red]%}?"

ZSH_THEME_GIT_PROMPT_PREFIX=""
ZSH_THEME_GIT_PROMPT_SUFFIX=" "
ZSH_THEME_GIT_PROMPT_DIRTY=""
ZSH_THEME_GIT_PROMPT_CLEAN=""

function prompt_char {
  if [ $UID -eq 0 ]; then echo "Λ"; else echo "λ"; fi
}

local ret_status="%(?:%{$fg_bold[cyan]%}$(prompt_char) :%{$fg_bold[red]%}$(prompt_char) %s)"

PROMPT='%{$fg_bold[yellow]%}%c%{$reset_color%} ${ret_status}%{$fg[white]%}'

RPROMPT='%{$fg[red]%}$(git_prompt_info)$(git_prompt_status)%{$fg[white]%}'
